﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CoinApp.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public List<Coin> Get(string coinAmount)
        {
            decimal amount = 0;
            decimal.TryParse(coinAmount, out amount);
            long cents = (long)(Math.Round(amount, 2) * 100);

            var coins = new Coin[] {
                new Coin() { Type = "$2", Value = 200 },
                new Coin() { Type = "$1", Value = 100 },
                new Coin() { Type = "50C", Value = 50 },
                new Coin() { Type = "20C", Value = 20 },
                new Coin() { Type = "10C", Value = 10 },
                new Coin() { Type = "5C", Value = 5 },
                new Coin() { Type = "2C", Value = 2 },
                new Coin() { Type = "1C", Value = 1 }
            };
            var combo = coins.Select(c => new Coin { Count = Math.DivRem(cents, c.Value, out cents), Value = c.Value, Type = c.Type })
                .Where(x => x.Count != 0).ToList();
            return combo;
        }
    }
}

public class Coin {
    public string Type { get; set; }
    public int Value { get; set; }
    public long? Count { get; set; }
}
