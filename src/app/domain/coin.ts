export interface Coin {
    type: string;
    value: number;
    count?: number;
}
