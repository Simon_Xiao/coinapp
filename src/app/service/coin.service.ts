import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Coin } from '../domain/coin';
import 'rxjs/add/operator/map';

@Injectable()
export class CoinService {

  constructor(private http: HttpClient) {
  }

  loadCoins(amount: string): Observable<Coin[]> {
    const params = new HttpParams().set('coinAmount', amount);
    return this.http.get('/api/values', {params}).map(res => res as Coin[]);
  }
}
