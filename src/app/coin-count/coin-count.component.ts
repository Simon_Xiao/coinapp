import { CoinService } from './../service/coin.service';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { switchMap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Coin } from '../domain/coin';

@Component({
  selector: 'app-coin-count',
  templateUrl: './coin-count.component.html',
  styleUrls: ['./coin-count.component.css']
})
export class CoinCountComponent implements AfterViewInit {
  coins: Coin[];
  @ViewChild('txtSearch') input;

  constructor(private coinService: CoinService) { }

  ngAfterViewInit() {
    const source = fromEvent(this.input.nativeElement as HTMLElement, 'keyup').pipe(
      debounceTime(500),
      distinctUntilChanged(),
      switchMap(() => this.coinService.loadCoins(this.input.nativeElement.value))
    );
    source.subscribe((val: Coin[]) => {
      this.coins = val;
    });
  }
}
