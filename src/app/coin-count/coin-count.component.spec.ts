import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoinCountComponent } from './coin-count.component';

describe('CoinCountComponent', () => {
  let component: CoinCountComponent;
  let fixture: ComponentFixture<CoinCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoinCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoinCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
